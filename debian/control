Source: containerd
Section: admin
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Tianon Gravi <tianon@debian.org>,
           Tim Potter <tpot@hpe.com>,
           Shengjing Zhu <zhsj@debian.org>,
           Reinhard Tartler <siretart@tauware.de>
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               go-md2man,
               golang-any,
               golang-github-blang-semver-dev,
               golang-github-container-orchestrated-devices-container-device-interface-dev (>= 0.8.0~),
               golang-github-containerd-btrfs-dev (>= 2),
               golang-github-containerd-cgroups-dev (>= 3),
               golang-github-containerd-console-dev,
               golang-github-containerd-continuity-dev (>= 0.0~git20190827.75bee3e~),
               golang-github-containerd-errdefs-dev (>= 0.3.0),
               golang-github-containerd-fifo-dev (>= 1.0.0~),
               golang-github-containerd-go-cni-dev (>= 1.1.9),
               golang-github-containerd-go-runc-dev (>= 1.1.0),
               golang-github-containerd-imgcrypt-dev,
               golang-github-containerd-log-dev,
               golang-github-containerd-nri-dev,
               golang-github-containerd-platforms-dev,
               golang-github-containerd-ttrpc-dev (>= 1.2),
               golang-github-containerd-typeurl-dev (>= 2),
               golang-github-containernetworking-plugins-dev,
               golang-github-containers-ocicrypt-dev,
               golang-github-coreos-bbolt-dev,
               golang-github-coreos-go-systemd-dev,
               golang-github-davecgh-go-spew-dev,
               golang-github-distribution-reference-dev,
               golang-github-docker-go-events-dev,
               golang-github-docker-go-metrics-dev,
               golang-github-docker-go-units-dev,
               golang-github-docker-spdystream-dev,
               golang-github-emicklei-go-restful-dev,
               golang-github-fsnotify-fsnotify-dev,
               golang-github-google-go-cmp-dev,
               golang-github-google-uuid-dev,
               golang-github-grpc-ecosystem-go-grpc-prometheus-dev,
               golang-github-hashicorp-go-multierror-dev,
               golang-github-imdario-mergo-dev,
               golang-github-intel-goresctrl-dev,
               golang-github-klauspost-compress-dev,
               golang-github-minio-sha256-simd-dev,
               golang-github-moby-locker-dev,
               golang-github-moby-sys-dev (>= 0.0~git20240807),
               golang-github-opencontainers-go-digest-dev (>= 1.0.0~),
               golang-github-opencontainers-image-spec-dev (>= 1.1.0~rc2-2~),
               golang-github-opencontainers-runtime-spec-dev,
               golang-github-opencontainers-runtime-tools-dev,
               golang-github-opencontainers-selinux-dev,
               golang-github-pelletier-go-toml-dev,
               golang-github-prometheus-client-golang-dev,
               golang-github-sirupsen-logrus-dev,
               golang-github-spf13-pflag-dev,
               golang-github-tchap-go-patricia-dev,
               golang-github-urfave-cli-dev (>= 1.22.2-4~),
               golang-github-vishvananda-netlink-dev,
               golang-go-zfs-dev,
               golang-golang-x-sync-dev,
               golang-golang-x-sys-dev,
               golang-google-grpc-dev (>= 1.30),
               golang-k8s-api-dev,
               golang-k8s-apimachinery-dev,
               golang-k8s-apiserver-dev,
               golang-k8s-client-go-dev,
               golang-k8s-klog-dev,
               golang-k8s-sigs-json-dev,
               golang-k8s-sigs-structured-merge-diff-dev,
               golang-k8s-sigs-yaml-dev,
               golang-k8s-utils-dev,
               golang-opentelemetry-otel-dev (>> 1.31),
               golang-tags.cncf-container-device-interface-dev
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://containerd.io/
Vcs-Git: https://salsa.debian.org/go-team/packages/containerd.git
Vcs-Browser: https://salsa.debian.org/go-team/packages/containerd
XS-Go-Import-Path: github.com/containerd/containerd
Testsuite: autopkgtest-pkg-go

Package: containerd
Architecture: linux-any
Breaks: docker.io (<< 1.12)
Depends: runc (>= 0.1.0~), ${misc:Depends}, ${shlibs:Depends}
Suggests: containernetworking-plugins
Built-Using: ${misc:Built-Using}
Description: open and reliable container runtime
 Containerd is an industry-standard container runtime with an emphasis on
 simplicity, robustness and portability. It is available as a daemon for
 Linux and Windows, which can manage the complete container life cycle of
 its host system: image transfer and storage, container execution and
 supervision, low-level storage and network attachments, etc.
 .
 Containerd is designed to be embedded into a larger system, rather than
 being used directly by developers or end-users.
 .
 This package contains the binaries.

Package: golang-github-containerd-containerd-dev
Architecture: all
Section: golang
Depends: golang-github-containerd-btrfs-dev (>= 2),
         golang-github-containerd-cgroups-dev (>= 3),
         golang-github-containerd-console-dev,
         golang-github-containerd-continuity-dev (>= 0.0~git20190827.75bee3e~),
         golang-github-containerd-errdefs-dev (>= 0.3.0),
         golang-github-containerd-fifo-dev (>= 1.0.0~),
         golang-github-containerd-go-cni-dev (>= 1.1.9),
         golang-github-containerd-go-runc-dev (>= 1.1.0),
         golang-github-containerd-imgcrypt-dev,
         golang-github-containerd-log-dev,
         golang-github-containerd-nri-dev,
         golang-github-containerd-platforms-dev,
         golang-github-containerd-ttrpc-dev (>= 1.2),
         golang-github-containerd-typeurl-dev (>= 2),
         golang-github-containernetworking-plugins-dev,
         golang-github-coreos-bbolt-dev,
         golang-github-distribution-reference-dev,
         golang-github-docker-go-events-dev,
         golang-github-docker-go-metrics-dev,
         golang-github-docker-go-units-dev,
         golang-github-google-uuid-dev,
         golang-github-grpc-ecosystem-go-grpc-prometheus-dev,
         golang-github-hashicorp-go-multierror-dev,
         golang-github-imdario-mergo-dev,
         golang-github-intel-goresctrl-dev,
         golang-github-klauspost-compress-dev,
         golang-github-minio-sha256-simd-dev,
         golang-github-moby-locker-dev,
         golang-github-moby-sys-dev (>= 0.0~git20240807),
         golang-github-opencontainers-go-digest-dev (>= 1.0.0~),
         golang-github-opencontainers-image-spec-dev (>= 1.1.0~rc2-2~),
         golang-github-opencontainers-runc-dev (>= 0.1.0~),
         golang-github-opencontainers-runtime-spec-dev,
         golang-github-opencontainers-selinux-dev,
         golang-github-pelletier-go-toml-dev,
         golang-github-prometheus-client-golang-dev,
         golang-github-sirupsen-logrus-dev,
         golang-github-tchap-go-patricia-dev,
         golang-github-urfave-cli-dev (>= 1.22.2~),
         golang-golang-x-sync-dev,
         golang-golang-x-sys-dev,
         golang-google-grpc-dev (>= 1.30),
         golang-k8s-api-dev,
         golang-k8s-apimachinery-dev,
         golang-k8s-apiserver-dev,
         golang-k8s-client-go-dev,
         golang-k8s-klog-dev,
         golang-k8s-utils-dev,
         golang-opentelemetry-otel-dev (>> 1.),
         golang-tags.cncf-container-device-interface-dev,
         ${misc:Depends}
Breaks: golang-github-containerd-imgcrypt-dev (<< 1.1.11-4),
        golang-github-docker-docker-dev (<< 26.1)
Description: open and reliable container runtime (development files)
 Containerd is an industry-standard container runtime with an emphasis on
 simplicity, robustness and portability. It is available as a daemon for
 Linux and Windows, which can manage the complete container life cycle of
 its host system: image transfer and storage, container execution and
 supervision, low-level storage and network attachments, etc.
 .
 Containerd is designed to be embedded into a larger system, rather than
 being used directly by developers or end-users.
 .
 This package provides development files.
